const setScrollSmooth = () => {
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({ behavior: 'smooth' });
        });
    });
}

//  For the typewriter
let sentence = [ " programming.", " vim!!!", " linux.", " C++.", " maths.", " F.R.I.E.N.D.S!", " myself." ];
let indexOfLastChar = 0, typeDelay = 100, clearDelay = typeDelay, toPrint = 0, untypeDelay, typeIt;
let stringToDisplay, typeText;
let target = document.getElementById("animatedText");

//  Function that types the characters

function type() {

    clearTimeout(typeIt);

    typeText = sentence[toPrint];
    indexOfLastChar++;
    stringToDisplay = typeText.substring(0, indexOfLastChar);

    if(stringToDisplay[stringToDisplay.length - 1] == '<') {
        while(stringToDisplay[stringToDisplay.length - 1] != '>') {
            indexOfLastChar++;
            stringToDisplay = typeText.substring(0, indexOfLastChar);
        }

        indexOfLastChar++;
        stringToDisplay = typeText.substring(0, indexOfLastChar);
    }

    //  Add a cursor at alternate calls (emulates actual typing animation)
    if(indexOfLastChar % 2)
        stringToDisplay += "|";

    target.innerHTML = stringToDisplay;

    typeIt = setTimeout(type, typeDelay);

    if(indexOfLastChar == typeText.length) {
        clearTimeout(typeIt);
        untypeDelay = setTimeout(unType, 3000);
    }
}

//  Function that removes the characters

function unType() {

    clearTimeout(untypeDelay);

    typeText = sentence[toPrint];
    indexOfLastChar--;
    stringToDisplay = typeText.substring(0, indexOfLastChar);

    if(stringToDisplay[stringToDisplay.length - 1] == '>') {
        while(stringToDisplay[stringToDisplay.length - 1] != '<') {
            indexOfLastChar--;
            stringToDisplay = typeText.substring(0, indexOfLastChar);
        }

        indexOfLastChar--;
        stringToDisplay = typeText.substring(0, indexOfLastChar);
    }

    //  Add a cursor at alternate calls (emulates actual typing animation)
    if(indexOfLastChar % 2)
        stringToDisplay += "|";

    target.innerHTML = stringToDisplay;

    clearIt = setTimeout(unType, clearDelay);

    if(indexOfLastChar == 0) {
        indexOfLastChar = 1;
        clearTimeout(clearIt);
        toPrint++;

        if(toPrint == sentence.length)
            toPrint = 0;

        typeIt = setTimeout(type, 750);
    }
}

//  End of section for typewriter

//  For smooth scrolling
setScrollSmooth();

//  typewriter starts
type();
